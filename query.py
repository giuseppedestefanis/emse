COMMENTS_BY_ISSUE_ID = """
    SELECT *
    FROM jira_issue_comment c
    WHERE c.issue_report_id = {}
"""

COMMENTS_BY_PROJECT_NAME = """
    SELECT c.*
    FROM jira_issue_report i, jira_issue_comment c
    WHERE i.project_name LIKE '{}' AND c.issue_report_id = i.id
"""

ISSUES_BY_PROJECT_NAME = """
    SELECT *
    FROM jira_issue_report i
    WHERE i.project_name LIKE '{}'
    LIMIT {}
"""

ISSUES_BY_PROJECT_NAME_AND_ASSIGNEE_ID = """
    SELECT *
    FROM jira_issue_report i
    WHERE i.project_name LIKE '{}' AND i.assignee_id = {}
"""

ISSUES_BY_KEYS = """
    SELECT *
    FROM jira_issue_report i
    WHERE i.key in {} OR ( i.project_name = '{}' AND i.assignee_id = {})
"""

ISSUE_BY_KEY = """
    SELECT *
    FROM jira_issue_report i
    WHERE i.key = '{}'
"""

DEVELPOPERS_BY_PROJECT_NAME = """
    SELECT DISTINCT(u.*)
    FROM jira_issue_report i, jira_user u
    WHERE i.project_name LIKE '{}' AND (i.assignee_id = u.id OR i.reporter_id = u.id)
"""

DEVELPOPER_BY_NAME = """
    SELECT *
    FROM jira_user u
    WHERE u.name LIKE '%%{}%%'
"""

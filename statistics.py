import re
from collections import defaultdict

import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

DATA_DIR = './data'
PROJECTS = [
    # 'Camel',
    # 'Derby',
    # 'Hadoop Common',
    # 'Hadoop HDFS',
    # 'HBase',
    # 'Hive',
    'Commons CLI',
    'Commons CSV',
    'Commons Math',
    'Commons Codec',
    'Commons JXPath',
    'Commons Collections',
    # 'Commons Lang',
]


def bug_fix_introducer_plot(df):
    topic_matches = df.MATCH_TOPIC_i_f.values
    dev_matches = df.MATCH_DEV_I_F.values
    print(np.nansum(dev_matches))
    raw_data = {
        'match': ['Developer', 'Topic'],
        'i_equals_f': [np.nansum(dev_matches), np.sum(topic_matches)],
        'i_not_equals_f': [len(dev_matches) - np.nansum(dev_matches), len(topic_matches) - np.sum(topic_matches)]
    }
    df = pd.DataFrame(raw_data, columns=['match', 'i_equals_f', 'i_not_equals_f'])
    # Create a figure with a single subplot
    f, ax = plt.subplots(1, figsize=(10, 5))
    # Set bar width at 1
    bar_width = 0.5
    # positions of the left bar-boundaries
    bar_l = [i for i in range(len(df['i_equals_f']))]
    # positions of the x-axis ticks (center of the bars as bar labels)
    tick_pos = [i + (bar_width / 4) for i in bar_l]
    # Create the total score for each participant
    totals = [i + j for i, j in zip(df['i_equals_f'], df['i_not_equals_f'])]
    # Create the percentage of the total score the dev value for each participant was
    pre_rel = [i / j * 100 for i, j in zip(df['i_equals_f'], totals)]
    # Create the percentage of the total score the topic value for each participant was
    mid_rel = [i / j * 100 for i, j in zip(df['i_not_equals_f'], totals)]
    # Create a bar chart in position bar_1
    ax.bar(bar_l,
           # using pre_rel data
           pre_rel,
           # labeled
           label='Introducer=Fixer',
           # with alpha
           alpha=0.9,
           # with color
           color='b',
           # with bar width
           width=bar_width,
           # with border color
           edgecolor='white'
           )
    # Create a bar chart in position bar_1
    ax.bar(bar_l,
           # using mid_rel data
           mid_rel,
           # with pre_rel
           bottom=pre_rel,
           # labeled
           label='Introducer!=Fixer',
           # with alpha
           alpha=0.9,
           # with color
           color='r',
           # with bar width
           width=bar_width,
           # with border color
           edgecolor='white'
           )
    # Set the ticks to be first names
    plt.xticks(tick_pos, df['match'])
    ax.set_ylabel("Percentage")
    ax.set_xlabel("")
    # Let the borders of the graphic
    plt.xlim([min(tick_pos) - bar_width, max(tick_pos) + bar_width])
    plt.ylim(-10, 110)
    # rotate axis labels
    plt.setp(plt.gca().get_xticklabels(), rotation=45, horizontalalignment='right')
    # shot plot
    plt.legend()
    plt.savefig('topic_dev_i_f.png')
    plt.show()


def dev_fix_introducer():
    dfs = []
    for project_name in PROJECTS:
        dfs.append(
            pd.read_csv(
                '{}/{}/dev_i_f_and_topics.csv'.format(DATA_DIR, project_name),
                delimiter=','
            )
        )
    df = pd.concat(dfs)
    dfs = []
    for project_name in PROJECTS:
        dfs.append(
            pd.read_csv(
                '{}/{}/commits.csv'.format(DATA_DIR, project_name),
                delimiter=',',
                names=['COMMIT_ID', 'DEV_NAME', 'DATE', 'COMMIT_MESSAGE'],
                encoding="ISO-8859-1",
                error_bad_lines=False
            )
        )
    df_commits = pd.concat(dfs).groupby(['DEV_NAME']).count()
    df_commits = (df_commits - df_commits.min()) / (df_commits.max() - df_commits.min())
    dfs = []
    for project_name in PROJECTS:
        dfs.append(
            pd.read_csv(
                '{}/{}/users_network_edges.csv'.format(DATA_DIR, project_name),
                delimiter=',',
                encoding="ISO-8859-1",
                error_bad_lines=False
            )
        )
    df_network = pd.concat(dfs).drop(['Id', 'Label', 'timeset'], axis=1)
    df_network = (df_network - df_network.min()) / (df_network.max() - df_network.min())
    df_i = df.groupby(['DEV_NAME_i']).count()
    df_i = (df_i - df_i.min()) / (df_i.max() - df_i.min())
    df_f = df.groupby(['DEV_NAME_f']).count()
    df_f = (df_f - df_f.min()) / (df_f.max() - df_f.min())

    fig, axes = plt.subplots(1, figsize=(10, 5))
    labels = ['Defect Introduced', 'Defect Fixed',
              '# Commits', 'Betweeness', 'Page Rank',
              'Modularity Class', 'In-Degree', 'Out-Degree']
    pos = range(0, len(labels))
    f_values = df_i['DEV_NAME_f'].values
    i_values = df_f['DEV_NAME_i'].values
    c_values = df_commits['COMMIT_ID'].values
    b_values = df_network['betweenesscentrality'].values
    p_values = df_network['pageranks'].values
    m_values = df_network['modularity_class'].values
    indegree_values = df_network['indegree'].values
    outdegree_values = df_network['outdegree'].values
    min = np.min([len(f_values), len(i_values)])
    axes.violinplot(
        [f_values, i_values, c_values, b_values, p_values, m_values, indegree_values, outdegree_values], pos,
        vert=False,
    )
    axes.set_yticks(np.arange(0, len(labels)))
    axes.set_yticklabels(labels)
    fig.suptitle("Developer experience")
    fig.subplots_adjust(hspace=0.4)
    plt.savefig('dev_metrics.png')
    plt.show()
    fig, ax = plt.subplots(1, figsize=(10, 5))
    # ax = fig.add_axes([0, 0, 1, 1])
    ax.scatter(f_values[:min], i_values[:min], color='b')
    ax.set_xlabel('Bug Fixing')
    ax.set_ylabel('Bug Introduction')
    ax.set_title('Fixing vs  Introduction')
    plt.savefig('fix_vs_introduction.png')
    plt.show()


def issues_topic_bug_heatmap(project_name):
    df = pd.read_csv(
        '{}/{}/dev_i_f_and_topics.csv'.format(DATA_DIR, project_name),
        delimiter=','
    )
    topic_matches = df.MATCH_TOPIC_i_f.values
    dev_matches = df.MATCH_DEV_I_F.values
    raw_data = {
        'match': ['Developer', 'Topic'],
        'i_equals_f': [np.nansum(dev_matches), np.sum(topic_matches)],
        'i_not_equals_f': [len(dev_matches) - np.nansum(dev_matches), len(topic_matches) - np.sum(topic_matches)]
    }
    matrix_data = np.matrix([raw_data['i_equals_f'], raw_data['i_not_equals_f']])
    matrix_data = matrix_data / matrix_data.sum() * 100
    sns.set_theme()
    sns.heatmap(
        matrix_data,
        yticklabels=['Topic Non Expert', 'Topic Expert'],
        xticklabels=['Introducer', 'Fixer'],
        annot=True,
        cbar_kws={'format': '%.0f%%'}
    )
    plt.savefig(f'topic_dev_i_f_heatmap_{project_name.replace(" ", "_").lower()}.png')
    plt.show()


def fixer_introducer_globalbar_plot():
    data = []
    for project_name in PROJECTS:
        df = pd.read_csv('{}/{}/dev_i_f_and_topics.csv'.format(DATA_DIR, project_name), delimiter=',')
        topic_matches = df.MATCH_TOPIC_i_f.values
        data.append([np.sum(topic_matches) / len(topic_matches) * 100,
                     (len(topic_matches) - np.sum(topic_matches)) / len(topic_matches) * 100,
                     project_name])
    # print(data)
    # sns.barplot(data=data)
    # plt.show()
    raw_data = {
        'project_name': [d[2] for d in data],
        'i_equals_f': [d[0] for d in data],
        'i_not_equals_f': [d[1] for d in data]
    }
    df = pd.DataFrame(raw_data, columns=['project_name', 'i_equals_f', 'i_not_equals_f'])
    df.plot(
        x='project_name',
        kind='bar',
        stacked=True,
        rot=20,
        figsize=(20, 10)
    )
    plt.legend(['Fixer and introducer are the same', 'Fixer and introducer are not the same'], loc="upper center")
    plt.savefig('topic_dev_i_f.png')
    plt.show()


def create_petri_nets_files():
    pattern = r'\S*@\S*\s?'
    pattern = re.compile(pattern)
    for project_name in PROJECTS:
        commits = pd.read_csv(
            '{}/{}/commits.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False
        )
        commits['who'] = commits.apply(lambda row: pattern.sub('', row.who).strip(), axis=1)
        commits = commits.groupby(['who']).count()
        ff = pd.read_csv(
            '{}/{}/ff2.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False,

        )
        ff['developer'] = ff.apply(lambda row: pattern.sub('', row.developer).strip(), axis=1)
        ff = ff.groupby(['developer', 'commit_id']).count()
        fi = pd.read_csv(
            '{}/{}/fi2.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False
        )
        fi['developer'] = fi.apply(lambda row: pattern.sub('', row.developer).strip(), axis=1)
        fi = fi.groupby(['developer', 'commit_id']).count()

        developers = commits.index.values
        dev_normal_commits, dev_fault_introduction_commits, dev_fault_fix_commits = [], [], []
        for developer in developers:
            dev_commits = commits.iloc[commits.index == developer].hash.values[0]
            fi_commits = len(fi.iloc[fi.index.get_level_values('developer') == developer])
            ff_commits = len(ff.iloc[ff.index.get_level_values('developer') == developer])
            normal_commits = dev_commits - ff_commits - fi_commits
            dev_fault_introduction_commits.append(fi_commits)
            dev_fault_fix_commits.append(ff_commits)
            dev_normal_commits.append(normal_commits if normal_commits > 0 else 0)

        raw_data = {
            'developer': developers,
            'normal_commits': dev_normal_commits,
            'fi_commits': dev_fault_introduction_commits,
            'ff_commits': dev_fault_fix_commits,
        }
        df = pd.DataFrame(raw_data, columns=['developer', 'normal_commits', 'fi_commits', 'ff_commits'])
        df.to_csv('{}/{}/petri_net_lambdas.csv'.format(DATA_DIR, project_name), index=False)


def generate_petri_net_parameters():
    global normal_commits, fi_commits, ff_commits, commits
    project_names, normal_commits, fi_commits, ff_commits = [], [], [], []
    for project_name in PROJECTS:
        commits = pd.read_csv(
            '{}/{}/petri_net_lambdas.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False
        )
        commits.num_commits = commits.normal_commits + commits.fi_commits + commits.ff_commits
        commits.normal_commits = commits.normal_commits / commits.num_commits
        commits.fi_commits = commits.fi_commits / commits.num_commits
        commits.ff_commits = commits.ff_commits / commits.num_commits
        project_names.append(project_name)
        normal_commits.append(commits.normal_commits.mean().round(decimals=3))
        fi_commits.append(commits.fi_commits.mean().round(decimals=3))
        ff_commits.append(commits.ff_commits.mean().round(decimals=3))
    raw_data = {
        'project': project_names,
        'normal_commits': normal_commits,
        'fi_commits': fi_commits,
        'ff_commits': ff_commits,
    }
    df = pd.DataFrame(raw_data, columns=['project', 'normal_commits', 'fi_commits', 'ff_commits'])
    print(df.to_latex(index=False))


def fault_introduction_fixing_timeseries_plot(project_name):
    sns.set_style("dark")
    sns.set_theme()
    for project_name in PROJECTS:
        f, ax = plt.subplots(1, figsize=(10, 5))
        commits = pd.read_csv(
            '{}/{}/commits.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False,

        )
        ff = pd.read_csv(
            '{}/{}/ff2.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False,

        )
        ff.issue_key = ff.apply(lambda row: f'{project_name.upper()}-{row.issue_key}', axis=1)
        ff = pd.merge(ff, commits, left_on='commit_id', right_on='hash', suffixes=('_i', '_f'))
        ff.drop(['developer', 'file_name', 'commit_id', 'hash', 'who'], inplace=True, axis=1)
        fi = pd.read_csv(
            '{}/{}/fi2.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False
        )
        fi.issue_key = fi.apply(lambda row: f'{project_name.upper()}-{row.issue_key}', axis=1)
        fi = pd.merge(fi, commits, left_on='commit_id', right_on='hash', suffixes=('_i', '_f'))
        fi.drop(['developer', 'file_name', 'commit_id', 'hash', 'who'], inplace=True, axis=1)
        i_f_topics = pd.read_csv(
            '{}/{}/dev_i_f_and_topics.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False
        )
        fi = pd.merge(fi, i_f_topics, left_on='issue_key', right_on='ISSUE_KEY', suffixes=('_i', '_f'))
        if 'Unnamed: 0' in fi.columns:
            fi.drop(['Unnamed: 0', 'ISSUE_KEY', 'DEV_NAME_i', 'MATCH_TOPIC_i_f',
                     'DEV_NAME_f', 'MATCH_DEV_I_F', 'DOMINANT_TOPIC_f'],
                    inplace=True, axis=1)
        else:
            fi.drop(['ISSUE_KEY', 'DEV_NAME_i', 'MATCH_TOPIC_i_f',
                     'DEV_NAME_f', 'MATCH_DEV_I_F', 'DOMINANT_TOPIC_f'],
                    inplace=True, axis=1)
        fi.DOMINANT_TOPIC_i = fi.apply(
            lambda row: int(row.DOMINANT_TOPIC_i.replace('DEV_TOPIC_', '').replace('_i', '')),
            axis=1)
        fi['match'] = fi.apply(lambda row: 0.1 if row.DOMINANT_TOPIC_i == row.ISSUE_TOPIC_i else 1, axis=1)
        print(fi.columns)
        print(fi.head())
        ff = pd.merge(ff, i_f_topics, left_on='issue_key', right_on='ISSUE_KEY', suffixes=('_i', '_f'))
        if 'Unnamed: 0' in ff.columns:
            ff.drop(['Unnamed: 0', 'ISSUE_KEY', 'DEV_NAME_i', 'MATCH_TOPIC_i_f',
                     'DEV_NAME_f', 'MATCH_DEV_I_F', 'DOMINANT_TOPIC_i'],
                    inplace=True, axis=1)
        else:
            ff.drop(['ISSUE_KEY', 'DEV_NAME_i', 'MATCH_TOPIC_i_f',
                     'DEV_NAME_f', 'MATCH_DEV_I_F', 'DOMINANT_TOPIC_i'],
                    inplace=True, axis=1)
        ff.DOMINANT_TOPIC_f = ff.apply(
            lambda row: int(row.DOMINANT_TOPIC_f.replace('DEV_TOPIC_', '').replace('_f', '')),
            axis=1)
        ff['match'] = ff.apply(lambda row: 1 if row.DOMINANT_TOPIC_f == row.ISSUE_TOPIC_i else 0.1, axis=1)
        print(ff.columns)
        fi.when = pd.to_datetime(fi['when'], utc=True)
        fi.index = pd.to_datetime(fi['when'], format='%Y-%m-%d %H:%M:%S')
        fi.drop(['issue_key', 'ISSUE_TOPIC_i', 'DOMINANT_TOPIC_i', 'when'],
                inplace=True, axis=1)
        fi = fi.groupby(pd.Grouper(freq='Y')).sum()
        fi.plot.area(ax=ax, stacked=False, color='red')
        ff.when = pd.to_datetime(ff['when'], utc=True)
        ff.index = pd.to_datetime(ff['when'], format='%Y-%m-%d %H:%M:%S')
        ff.drop(['issue_key', 'ISSUE_TOPIC_i', 'DOMINANT_TOPIC_f', 'when'],
                inplace=True, axis=1)
        ff = ff.groupby(pd.Grouper(freq='Y')).sum()
        ff.plot.area(ax=ax, stacked=False, color='green')
        plt.legend(['No Match Topic Introducer-Fault', 'No Match Topic Fixer-Fault'], loc="upper right")
        plt.savefig(f'topic_dev_i_f_timeseries_{project_name.replace(" ", "_")}.png')
        plt.xlabel('')
        plt.show()


def projects_statistics():
    raw_data = defaultdict(list)
    pattern = r'\S*@\S*\s?'
    pattern = re.compile(pattern)
    for project_name in PROJECTS:
        raw_data['project name'].append(project_name)
        df = pd.read_csv(
            '{}/{}/commits.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False,

        )
        raw_data['# commits'].append(len(df))
        developers = len(set(df.who.values))
        df.when = pd.to_datetime(df.when, utc=True)
        raw_data['first commit'].append(min(df.when.dt.strftime('%Y-%m-%d').values))
        raw_data['last commit'].append(max(df.when.dt.strftime('%Y-%m-%d').values))
        df.who_name = df.apply(lambda row: pattern.sub('', row.who).strip(), axis=1)
        unique_devs = len(set(df.who_name.values))
        raw_data['% aliases'].append(100 - unique_devs / developers * 100)
        df = pd.read_csv(
            '{}/{}/fi2.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False,

        )
        raw_data['# FI'].append(len(set(df.issue_key.values)))
        df = pd.read_csv(
            '{}/{}/ff2.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False,

        )
        raw_data['# FF'].append(len(set(df.issue_key.values)))
        df = pd.read_csv(
            '{}/{}/bugs.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False,

        )
        raw_data['# Bugs'].append(len(df))
        df = pd.read_csv(
            '{}/{}/git.csv'.format(DATA_DIR, project_name),
            delimiter=',',
            encoding="ISO-8859-1",
            error_bad_lines=False,

        )
        raw_data['# commiters'].append(len(set(df.Commiter)))
        raw_data['# authors'].append(len(set(df.Author)))

    df = pd.DataFrame(raw_data,
                      columns=['project name', '# commits', '# FI', '# FF', '# Bugs', '# commiters',
                               '# authors', 'first commit', 'last commit', '% aliases'])
    print(df.drop(['% aliases'], axis=1).to_latex(index=False))
    print(df.to_latex(index=False))
    print(df['% aliases'].values.mean())


if __name__ == '__main__':
    # for project_name in PROJECTS:
    #     issues_topic_bug_heatmap(project_name)
    # fixer_introducer_globalbar_plot()
    # create_petri_nets_files()
    # generate_petri_net_parameters()
    # fault_introduction_fixing_timeseries_plot()
    projects_statistics()

import sys
from pprint import pprint

import gensim
import matplotlib.colors as mcolors
import numpy as np
import pandas as pd
import pyLDAvis.gensim
from bokeh.io import output_notebook
from bokeh.plotting import figure, output_file, save
from gensim import corpora
from sklearn.manifold import TSNE
import csv
import re
from main_issues import DATA_DIR, sent_to_words, process_words, format_topics_sentences

csv.field_size_limit(sys.maxsize)


def visualize_topics(lda_model, corpus, project_name):
    vis = pyLDAvis.gensim.prepare(lda_model, corpus, dictionary=lda_model.id2word, mds='mmds')
    file_name = '{}/{}/topic_modeling_visualization_for_libraries.html'.format(DATA_DIR, project_name.replace('/', ' '))
    pyLDAvis.save_html(vis, file_name)


def show_topic_clusters(lda_model, corpus, project_name):
    topic_weights = []
    for i, row_list in enumerate(lda_model[corpus]):
        topic_weights.append([w for i, w in row_list[0]])

    # Array of topic weights
    arr = pd.DataFrame(topic_weights).fillna(0).values

    # Keep the well separated points (optional)
    arr = arr[np.amax(arr, axis=1) > 0.35]

    # Dominant topic number in each doc
    topic_num = np.argmax(arr, axis=1)

    # tSNE Dimension Reduction
    tsne_model = TSNE(n_components=2, verbose=1, random_state=0, angle=.99, init='pca')
    tsne_lda = tsne_model.fit_transform(arr)

    # Plot the Topic Clusters using Bokeh
    output_notebook()
    file_name = '{}/{}/topic_modeling_for_libraries.html'.format(DATA_DIR, project_name.replace('/', ' '))
    output_file(file_name)

    mycolors = np.array([color for name, color in mcolors.TABLEAU_COLORS.items()])
    plot = figure(title="t-SNE Clustering of {} LDA Topics for {}".format(n_topics, project_name),
                  plot_width=900, plot_height=700)
    plot.scatter(x=tsne_lda[:, 0], y=tsne_lda[:, 1], color=mycolors[topic_num])
    save(plot)


if __name__ == '__main__':
    n_topics = 10
    projects = [
        'HBase',
        # 'Hive', 'ZooKeeper', 'Camel', 'Cassandra', 'Solr', 'Hibernate ORM',
        # 'Derby', 'Hadoop Common', 'Hadoop HDFS', 'Wicket', 'GeoServer',
        # 'Geronimo', 'Pig',  'Hadoop Map/Reduce', 'JRuby', 'OFBiz'

    ]

    for project_name in projects:
        try:
            # Import Dataset
            data = []
            with open('{}/{}/imports.csv'.format(DATA_DIR, project_name), 'r')as csv_file:
                reader = csv.reader(csv_file, delimiter=',')
                for row in reader:
                    imports = row[2:]
                    imports = " ".join([imp for imp in imports if imp])
                    imports = " ".join(re.split('\\.|,|;', imports))
                    data.append(imports)

            print(len(data))
            data_words = list(sent_to_words(data))
            print(data_words[:1])
            # Build the bigram and trigram models
            bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100)  # higher threshold fewer phrases.
            trigram = gensim.models.Phrases(bigram[data_words], threshold=100)
            bigram_mod = gensim.models.phrases.Phraser(bigram)
            trigram_mod = gensim.models.phrases.Phraser(trigram)

            data_ready = process_words(data_words, bigram_mod=bigram_mod,
                                       trigram_mod=trigram_mod)  # processed Text Data!

            # Create Dictionary
            id2word = corpora.Dictionary(data_ready)

            # Create Corpus: Term Document Frequency
            corpus = [id2word.doc2bow(text) for text in data_ready]

            # Build LDA model
            lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                                        id2word=id2word,
                                                        num_topics=n_topics,
                                                        random_state=100,
                                                        update_every=1,
                                                        chunksize=10,
                                                        passes=10,
                                                        alpha='symmetric',
                                                        iterations=100,
                                                        per_word_topics=True)

            pprint(lda_model.print_topics())
            df_topic_sents_keywords = format_topics_sentences(ldamodel=lda_model, corpus=corpus, texts=data_ready)

            # Format
            df_dominant_topic = df_topic_sents_keywords.reset_index()
            df_dominant_topic.columns = ['Document_No', 'Dominant_Topic', 'Topic_Perc_Contrib', 'Keywords', 'Text']
            print(df_dominant_topic.head(10))

            # Get topic weights
            show_topic_clusters(lda_model, corpus, project_name)
            visualize_topics(lda_model, corpus, project_name)
        except Exception as e:
            print(e)
